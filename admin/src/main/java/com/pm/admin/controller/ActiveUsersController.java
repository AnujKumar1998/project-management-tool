package com.pm.admin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pm.admin.entity.User;
import com.pm.admin.feignClient.UserClient;
import com.pm.admin.services.AdminService;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = "/admin/active-users")
public class ActiveUsersController {
   
	@Autowired
	private AdminService adminservice;
	
	@Autowired
	private UserClient userclient;
	
	@GetMapping(value = "/getall")
	public ResponseEntity<List<User>> getAllTheUser()
	{
		List<User> user=adminservice.getAllTheUsers();
		return new ResponseEntity<List<User>>(user,HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<User> getTheUserById(@PathVariable Long id)
	{
	        User user=adminservice.getUserById(id);
	     
		return new ResponseEntity<User>(user,HttpStatus.OK);
	}
	
	@GetMapping(value = "/",params = "name")
	public ResponseEntity<User> getTheUserByName(@RequestParam("name") String name)
	{
	        User user=adminservice.getUserByName(name);
		return new ResponseEntity<User>(user,HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/delete/{id}")
	public ResponseEntity<Void> deleteTheUser(@PathVariable Long id)
	{       userclient.deleteTheUser(id);
	        adminservice.deleteTheUser(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@GetMapping(value = "/search/{keyword}")
	public ResponseEntity<List<User>> search(@PathVariable String keyword)
	{
		List<User> user=adminservice.searchTheUsers(keyword);
		return new ResponseEntity<List<User>>(user,HttpStatus.OK);
	}
}
