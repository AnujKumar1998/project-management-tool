package com.pm.admin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pm.admin.entity.User;
import com.pm.admin.feignClient.UserClient;
import com.pm.admin.services.AdminService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "/admin/non-active-users")
public class NonActiveUsersController {
      
	@Autowired
	private UserClient userclient;
	
	@Autowired
	private AdminService adminservice;
	
	@GetMapping(value = "/allusers")
	public ResponseEntity<List<User>> getAllTheUser()
	{
		List<User> user=userclient.getAllUsers();
		return new ResponseEntity<List<User>>(user,HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<User> getTheUserById(@PathVariable Long id)
	{
	        User user=userclient.getUserById(id);
		return new ResponseEntity<User>(user,HttpStatus.OK);
	}
	
	@GetMapping(value = "/{name}")
	public ResponseEntity<User> getTheUserByName(@PathVariable String name)
	{
	        User user=userclient.getUserByName(name);
		return new ResponseEntity<User>(user,HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/delete/{id}")
	public ResponseEntity<Void> getTheUser(@PathVariable Long id)
	{
	    userclient.deleteTheUser(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@GetMapping(value = "/search/{keyword}")
	public ResponseEntity<List<User>> search(@PathVariable String keyword)
	{
		List<User> user=userclient.searchTheUsers(keyword);
		return new ResponseEntity<List<User>>(user,HttpStatus.OK);
	}
	
	@PostMapping(value="/save/{id}")
	public ResponseEntity<User> saveTheUser(@PathVariable Long id)
	{      
	        User user=userclient.getUserById(id);
	        
		return new ResponseEntity<User>(adminservice.saveTheUSer(user),HttpStatus.OK);
	}
}
