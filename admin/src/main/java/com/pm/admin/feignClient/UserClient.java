package com.pm.admin.feignClient;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.pm.admin.entity.User;

@FeignClient(name="Login-Signup")
public interface UserClient {
      
	@GetMapping(value = "/api")
	public List<User> getAllUsers();

	@GetMapping(value = "/api/byid/{id}")
	public User getUserById(@PathVariable("id") Long id);

	@GetMapping(value = "/api/byname/{name}")
	public User getUserByName(@PathVariable("name") String name);

	@DeleteMapping(value = "/api/delete/{id}")
	public void deleteTheUser(@PathVariable("id") Long id);

	@GetMapping(value = "/api/search/{keyword}")
    public List<User> searchTheUsers(@PathVariable("keyword") String keyword);
}
