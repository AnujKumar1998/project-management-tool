package com.pm.admin.services;

import java.util.List;

import com.pm.admin.entity.User;

public interface AdminService {
	 public List<User> getAllTheUsers();
     public User getUserById(Long id);
     public User getUserByName(String name);
     public void deleteTheUser(Long id);
     public List<User> searchTheUsers(String keyword);
     
     public User saveTheUSer(User user);
}
