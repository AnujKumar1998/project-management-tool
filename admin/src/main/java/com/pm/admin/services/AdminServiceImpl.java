package com.pm.admin.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pm.admin.entity.User;
import com.pm.admin.feignClient.UserClient;
import com.pm.admin.repository.UserRepository;

@Service
@Transactional
public class AdminServiceImpl implements AdminService {
    
	@Autowired
	private UserRepository userrepository;
	
	@Autowired
	private UserClient userclient;
	
	@Override
	public List<User> getAllTheUsers() {
		return userrepository.findAll();
	}

	@Override
	public User getUserById(Long id) {
		return userrepository.findById(id).get();
	}

	@Override
	public User getUserByName(String name) {
		return userrepository.findByName(name);
	}

	@Override
	public void deleteTheUser(Long id) {
		  userclient.deleteTheUser(id);
          userrepository.deleteById(id);		
	}

	@Override
	public List<User> searchTheUsers(String keyword) {
		return userrepository.searchTheUsers(keyword);
	}

	@Override
	public User saveTheUSer(User user) {
		return userrepository.save(user);
	}

}
