package com.pm.createproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication(scanBasePackages = {"com.pm.createproject.*"})
@EntityScan(basePackages = {"com.pm.createproject.entity"})
@EnableJpaRepositories(basePackages = {"com.pm.createproject.repository"})
@EnableFeignClients
public class CreateprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(CreateprojectApplication.class, args);
	}

}
