package com.pm.createproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pm.createproject.services.Projectservices;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/projects")
public class DashboardController {
   
	@Autowired
	private Projectservices projectService;
	
	@GetMapping("/total-projects-assigned-by-you/{assigned_by}")
	public Long TotalProjectsAssignedByYou(@PathVariable String assigned_by) {
	    return projectService.TotalProjectsAssignedByYou(assigned_by);
	}
	
	
	@GetMapping("/todo-projects-assigned-by-you/{assigned_by}")
	public Long ToDoAssignedByYou(@PathVariable String assigned_by) {
	    return projectService.ToDoProjectsAssignedByYou(assigned_by);
	}
	

	@GetMapping("/ontrack-projects-assigned-by-you/{assigned_by}")
	public Long OnTrackAssignedByYou(@PathVariable String assigned_by) {
	    return projectService.OnTrackProjectsAssignedByYou(assigned_by);
	}
	

	@GetMapping("/pending-projects-assigned-by-you/{assigned_by}")
	public Long pendingAssignedByYou(@PathVariable String assigned_by) {
	    return projectService.PendingProjectsAssignedByYou(assigned_by);
	}
	

	@GetMapping("/complete-projects-assigned-by-you/{assigned_by}")
	public Long CompletedAssignedByYou(@PathVariable String assigned_by) {
	    return projectService.CompletedProjectsAssignedByYou(assigned_by);
	}
	
	

	@GetMapping("/total-projects-assigned-to-you/{email}")
	public Long TotalProjectsAssignedToYou(@PathVariable String email) {
	    return projectService.TotalProjectsAssignedToYou(email);
	}
	
	
	@GetMapping("/todo-projects-assigned-to-you/{email}")
	public Long ToDoAssignedToYou(@PathVariable String email) {
	    return projectService.ToDoProjectsAssignedToYou(email);
	}
	

	@GetMapping("/ontrack-projects-assigned-to-you/{email}")
	public Long OnTrackAssignedToYou(@PathVariable String email) {
	    return projectService.OnTrackProjectsAssignedToYou(email);
	}
	

	@GetMapping("/pending-projects-assigned-to-you/{email}")
	public Long pendingAssignedToYou(@PathVariable String email) {
	    return projectService.PendingProjectsAssignedToYou(email);
	}
	

	@GetMapping("/complete-projects-assigned-to-you/{email}")
	public Long CompletedAssignedToYou(@PathVariable String email) {
	    return projectService.CompletedProjectsAssignedToYou(email);
	}
	
	
	
}
