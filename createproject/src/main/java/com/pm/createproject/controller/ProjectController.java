package com.pm.createproject.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pm.createproject.entity.Project;
import com.pm.createproject.headers.HeaderGenerator;
import com.pm.createproject.services.Projectservices;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = "/projects")
public class ProjectController {
	@Autowired
	private Projectservices projectService;

	@Autowired
	private HeaderGenerator headerGenerator;

	// Projects------------------------------------------>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	@GetMapping(value = "")
	public ResponseEntity<List<Project>> getAllProjects() {
		List<Project> projects = projectService.getAllProjects();
		if (!projects.isEmpty()) {
			return new ResponseEntity<List<Project>>(projects, headerGenerator.getHeadersForSuccessGetMethod(),
					HttpStatus.OK);
		}
		return new ResponseEntity<List<Project>>(headerGenerator.getHeadersForError(), HttpStatus.NOT_FOUND);
	}

	@GetMapping(value = "/", params = "assigned_to")
	public ResponseEntity<List<Project>> getAllProjectsAssignedTo(@RequestParam String assigned_to) {
		List<Project> projects = projectService.allProjectsAssignedToYou(assigned_to);
		if (!projects.isEmpty()) {
			return new ResponseEntity<List<Project>>(projects, headerGenerator.getHeadersForSuccessGetMethod(),
					HttpStatus.OK);
		}
		return new ResponseEntity<List<Project>>(headerGenerator.getHeadersForError(), HttpStatus.NOT_FOUND);
	}

	@GetMapping(value = "/", params = "assigned_by")
	public ResponseEntity<List<Project>> getAllProjectsAssignedBy(@RequestParam String assigned_by) {
		List<Project> projects = projectService.allProjectsAssignedByYou(assigned_by);
		if (!projects.isEmpty()) {
			return new ResponseEntity<List<Project>>(projects, headerGenerator.getHeadersForSuccessGetMethod(),
					HttpStatus.OK);
		}
		return new ResponseEntity<List<Project>>(headerGenerator.getHeadersForError(), HttpStatus.NOT_FOUND);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<Project> getProject(@PathVariable("id") Long id) {
		Project project = projectService.getProjectById(id);

		if (project != null) {
			return new ResponseEntity<Project>(project, headerGenerator.getHeadersForSuccessGetMethod(), HttpStatus.OK);
		}
		return new ResponseEntity<Project>(headerGenerator.getHeadersForError(), HttpStatus.NOT_FOUND);

	}

	@GetMapping(value = "/", params = "name")
	public ResponseEntity<Project> getProjectByName(@RequestParam("name") String name) {
		Project project = projectService.getProjectByName(name);
		if (project != null) {
			return new ResponseEntity<Project>(project, headerGenerator.getHeadersForSuccessGetMethod(), HttpStatus.OK);
		}
		return new ResponseEntity<Project>(headerGenerator.getHeadersForError(), HttpStatus.NOT_FOUND);
	}

	@PostMapping(value = "/add")
	public ResponseEntity<Project> addProject(@RequestBody Project project, HttpServletRequest request) {

		if (project != null)
			try {
				projectService.saveProject(project);
				return new ResponseEntity<Project>(project,
						headerGenerator.getHeadersForSuccessPostMethod(request, project.getId()), HttpStatus.CREATED);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<Project>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		return new ResponseEntity<Project>(HttpStatus.BAD_REQUEST);

	}

	@DeleteMapping(value = "/delete/{id}")
	public ResponseEntity<Void> removeProject(@PathVariable("id") Long id) {
		Project project = projectService.getProjectById(id);

		if (project != null) {
			try {

				projectService.deleteProject(id);

				return new ResponseEntity<Void>(headerGenerator.getHeadersForSuccessGetMethod(), HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<Void>(headerGenerator.getHeadersForError(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		return new ResponseEntity<Void>(headerGenerator.getHeadersForError(), HttpStatus.NOT_FOUND);

	}

	@PutMapping(value = "/{id}", params = "completion_percent")
	private ResponseEntity<Void> updateCompletionPercent(@RequestParam("completion_percent") Integer completion_percent,
			@PathVariable Long id) {

		Project project = projectService.getProjectById(id);

		if (project != null) {
			try {
				projectService.updateProjectCompletionPercent(completion_percent, id);
				return new ResponseEntity<Void>(headerGenerator.getHeadersForSuccessGetMethod(), HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<Void>(headerGenerator.getHeadersForError(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		return new ResponseEntity<Void>(headerGenerator.getHeadersForError(), HttpStatus.NOT_FOUND);

	}

	
	@GetMapping("/search")
	private List<Project> Search(@RequestParam String keyword) {
		return projectService.Search(keyword);
	}

}
