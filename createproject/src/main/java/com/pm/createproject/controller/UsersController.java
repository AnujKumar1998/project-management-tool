package com.pm.createproject.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pm.createproject.entity.AssignedTo;
import com.pm.createproject.feignClient.UserServiceClient;
import com.pm.createproject.headers.HeaderGenerator;
import com.pm.createproject.services.AssignedtoService;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = "/projects/users-invited-to-project")
public class UsersController {
	@Autowired
	private AssignedtoService userService;

	@Autowired
	private HeaderGenerator headerGenerator;

	@Autowired
	private UserServiceClient userserviceclient;

	@GetMapping(value = "")
	public ResponseEntity<List<AssignedTo>> getAllUsers() {
		return userserviceclient.getAllTheUsers();
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<AssignedTo> getUser(@PathVariable("id") Long id) {
		return userserviceclient.getTheUserById(id);
	}

	@GetMapping(value = "/", params = "name")
	public ResponseEntity<AssignedTo> getUser(@RequestParam("name") String name) {
		return userserviceclient.getTheUserByName(name);

	}

	@PostMapping(value = "/save/{id}")
	public ResponseEntity<AssignedTo> addUser(@PathVariable Long id, HttpServletRequest request) {
		AssignedTo assignedTo = userserviceclient.getTheUserById(id).getBody();
		if (assignedTo != null)
			try {
				userService.saveTheUser(assignedTo);
				return new ResponseEntity<AssignedTo>(assignedTo,
						headerGenerator.getHeadersForSuccessPostMethod(request, assignedTo.getId()), HttpStatus.CREATED);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<AssignedTo>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		return new ResponseEntity<AssignedTo>(HttpStatus.BAD_REQUEST);

	}

	@DeleteMapping(value = "/delete/{id}")
	public ResponseEntity<Void> removeUser(@PathVariable("id") Long id) {
		AssignedTo assignedTo = userService.getUserById(id);
		if (assignedTo != null) {
			try {
				userService.deleteTheUser(id);
				return new ResponseEntity<Void>(headerGenerator.getHeadersForSuccessGetMethod(), HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<Void>(headerGenerator.getHeadersForError(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		return new ResponseEntity<Void>(headerGenerator.getHeadersForError(), HttpStatus.NOT_FOUND);

	}

	@GetMapping(value = "/search/{keyword}")
	public ResponseEntity<List<AssignedTo>> searchTheUsers(@PathVariable String keyword) {
		return userserviceclient.searchTheUsers(keyword);
	}

}
