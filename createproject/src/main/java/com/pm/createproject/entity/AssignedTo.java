package com.pm.createproject.entity;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Assignedto")
public class AssignedTo {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotBlank
	@Size(max = 20)
	private String username;

	@NotBlank
	@Size(max = 50)
	private String name;

	@NotBlank
	@Size(max = 50)
	@Email
	private String email;

	private Long mobno;

	@NotBlank
	@Size(max = 50)
	private String dept;

	@NotBlank
	@Size(max = 50)
	private String empid;

	@NotBlank
	@Size(max = 120)
	private String password;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "roles_assignedto", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<RoleAssignedTo> roleAssignedTos = new HashSet<>();
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name="invited_users",joinColumns = @JoinColumn(name = "pid"),inverseJoinColumns = @JoinColumn(name="uid"))
    @JsonIgnore
	private List<Project> project;

	public AssignedTo() {
	}

	

	public AssignedTo(Long id, @NotBlank @Size(max = 20) String username, @NotBlank @Size(max = 50) String name,
			@NotBlank @Size(max = 50) @Email String email, Long mobno, @NotBlank @Size(max = 50) String dept,
			@NotBlank @Size(max = 50) String empid, @NotBlank @Size(max = 120) String password, Set<RoleAssignedTo> roleAssignedTos,
			List<Project> project) {
		super();
		this.id = id;
		this.username = username;
		this.name = name;
		this.email = email;
		this.mobno = mobno;
		this.dept = dept;
		this.empid = empid;
		this.password = password;
		this.roleAssignedTos = roleAssignedTos;
		this.project = project;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getMobno() {
		return mobno;
	}

	public void setMobno(Long mobno) {
		this.mobno = mobno;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getEmpid() {
		return empid;
	}

	public void setEmpid(String empid) {
		this.empid = empid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<RoleAssignedTo> getRoles() {
		return roleAssignedTos;
	}

	public void setRoles(Set<RoleAssignedTo> roleAssignedTos) {
		this.roleAssignedTos = roleAssignedTos;
	}



	public List<Project> getProject() {
		return project;
	}



	public void setProject(List<Project> project) {
		this.project = project;
	}
	
	

}
