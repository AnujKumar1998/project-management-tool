package com.pm.createproject.entity;

public enum ERoleAssignedBy {
	ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
