package com.pm.createproject.entity;

public enum ERoleAssignedTo {
	ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
