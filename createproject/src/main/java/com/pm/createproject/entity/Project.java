package com.pm.createproject.entity;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ManyToAny;



@Entity
@Table(name = "projects")
public class Project {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@Column(name = "created_on")
	private Date created_on;

	@Column(name = "start_date")
	private Date start_date;
     
	@Column(name = "deadline")
	private Date deadline;

    
	
	@Column(name = "completion_percent")
	private Integer completion_percent;
	
    @ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private List<AssignedTo> assigned_to;
    
    @ManyToOne(fetch = FetchType.EAGER,cascade = 
    		CascadeType.ALL)
    @JoinColumn(name = "u_id")
	private AssignedBy assigned_by;
	
	public Project() {
		
	}

	public Project(Long id, String name, String description, Date created_on, Date start_date, Date deadline,
			AssignedBy assigned_by, Integer completion_percent, List<AssignedTo> assigned_to) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.created_on = created_on;
		this.start_date = start_date;
		this.deadline = deadline;
		this.assigned_by = assigned_by;
		this.completion_percent = completion_percent;
		this.assigned_to = assigned_to;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreated_on() {
		return created_on;
	}

	public void setCreated_on(Date created_on) {
		this.created_on = created_on;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public AssignedBy getAssigned_by() {
		return assigned_by;
	}

	public void setAssigned_by(AssignedBy assigned_by) {
		this.assigned_by = assigned_by;
	}

	public Integer getCompletion_percent() {
		return completion_percent;
	}

	public void setCompletion_percent(Integer completion_percent) {
		this.completion_percent = completion_percent;
	}

	public List<AssignedTo> getAssigned_to() {
		return assigned_to;
	}

	public void setAssigned_to(List<AssignedTo> assigned_to) {
		this.assigned_to = assigned_to;
	}

	@Override
	public String toString() {
		return "Project [id=" + id + ", name=" + name + ", description=" + description + ", created_on=" + created_on
				+ ", start_date=" + start_date + ", deadline=" + deadline + ", assigned_by=" + assigned_by
				+ ", completion_percent=" + completion_percent + ", assigned_to=" + assigned_to + "]";
	}

	
}
