package com.pm.createproject.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "AssignedBy_roles")
public class RoleAssignedBy {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Enumerated(EnumType.STRING)
	@Column(length = 20)
	private ERoleAssignedTo name;

	public RoleAssignedBy() {

	}

	public RoleAssignedBy(ERoleAssignedTo name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ERoleAssignedTo getName() {
		return name;
	}

	public void setName(ERoleAssignedTo name) {
		this.name = name;
	}
}