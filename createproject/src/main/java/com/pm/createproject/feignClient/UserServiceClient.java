package com.pm.createproject.feignClient;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.pm.createproject.entity.AssignedTo;


@FeignClient(name = "Users")
public interface UserServiceClient {
	
	@GetMapping(value = "/users/invited-users")
	public ResponseEntity<List<AssignedTo>> getAllTheUsers();

	@GetMapping(value = "/users/invited-users/{id}")
	public ResponseEntity<AssignedTo> getTheUserById(@PathVariable("id") Long id);

	@GetMapping(value = "/users/invited-users/", params = "name")
	public ResponseEntity<AssignedTo> getTheUserByName(@RequestParam("name") String name);

	@DeleteMapping(value = "/users/invited-users/delete/{id}")
	public ResponseEntity<Void> deleteTheUser(@PathVariable("id") Long id);

	@GetMapping(value = "/users/invited-users/search/{keyword}")
    public ResponseEntity<List<AssignedTo>> searchTheUsers(@PathVariable("keyword") String keyword);
}
