package com.pm.createproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pm.createproject.entity.AssignedBy;

@Repository
public interface AssignedbyRepository extends JpaRepository<AssignedBy, Long> {
	AssignedBy findByName(String name);

	@Query(value = "SELECT * FROM users u WHERE CONCAT(u.username,u.name,u.email,u.mobno,u.dept,u.empid) LIKE %?1%", nativeQuery = true)
	public List<AssignedBy> searchTheUsers(String keyword);
}
