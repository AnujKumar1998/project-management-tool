package com.pm.createproject.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pm.createproject.entity.AssignedTo;

@Repository
public interface AssignedtoRepository extends JpaRepository<AssignedTo, Long> {
	Optional<AssignedTo> findByUsername(String username);

	AssignedTo findByName(String name);

	@Query(value = "SELECT * FROM users u WHERE CONCAT(u.username,u.name,u.email,u.mobno,u.dept,u.empid) LIKE %?1%", nativeQuery = true)
	public List<AssignedTo> searchTheUsers(String keyword);
}
