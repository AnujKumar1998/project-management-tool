package com.pm.createproject.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pm.createproject.entity.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
	public Project findByName(String name);
	

	@Query(value = "SELECT * FROM projects p, assignedto u, projects_assigned_to z WHERE u.email=?1 AND z.project_id=p.id AND z.assigned_to_id=u.id", nativeQuery = true)
	public List<Project> allProjectsAssignedToYou(String email);

	@Query(value = "SELECT * FROM projects p,assignedby u WHERE u.email=?1 AND u.id=p.u_id", nativeQuery = true)
	public List<Project> allProjectsAssignedByYou(String email);

	@Modifying
	@Query(value = "UPDATE projects p SET p.completion_percent=:completion_percent WHERE p.id=:id", nativeQuery = true)
	void updateProjectCompletionPercent(Integer completion_percent, Long id);

	@Modifying
	@Query(value = "UPDATE projects p SET p.name=?1 p.description=?2 p.created_on=?3 p.start_date=?4 p.deadline=?5 p.assigned_to=?6 p.assigned_by=?7 p.completion_percent=?8 WHERE p.id =?9", nativeQuery = true)
	void updateProject(String name, String description, Date created_on, Date start_date, Date deadline,
			String assigned_to, String assigned_by, Integer completion_percent, Long id);

	@Query(value = "SELECT * FROM projects p WHERE CONCAT(p.name, p.description, p.created_on, p.start_date,p.deadline) LIKE %?1%", nativeQuery = true)
	public List<Project> search(String keyword);

	// Dashboard
	// ByYou
	@Query(value = "SELECT COUNT(*) FROM projects p,assignedby u WHERE u.email=?1 AND u.id=p.u_id", nativeQuery = true)
	public Long TotalProjectsAssignedByYou(String email);

	@Query(value = "SELECT COUNT(*) FROM projects p,assignedby u  WHERE (select cast(now() as date))< p.start_date AND p.completion_percent=0 AND u.email=?1 AND u.id=p.u_id", nativeQuery = true)
	public Long ToDoAssignedByYou(String email);

	@Query(value = "SELECT COUNT(*) FROM projects p,assignedby u  WHERE (select cast(now() as date))>= p.start_date AND (select cast(now() as date))<= p.deadline AND p.completion_percent>0 AND p.completion_percent<100 AND u.email=?1 AND u.id=p.u_id", nativeQuery = true)
	public Long OnTrackAssignedByYou(String email);

	@Query(value = "SELECT COUNT(*) FROM projects p,assignedby u  WHERE p.completion_percent=100 AND u.email=?1 AND u.id=p.u_id", nativeQuery = true)
	public Long CompletedAssignedByYou(String email);

	@Query(value = "SELECT COUNT(*) FROM projects p,assignedby u  WHERE (select cast(now() as date)) >= p.start_date AND p.completion_percent=0 AND u.email=?1 AND u.id=p.u_id", nativeQuery = true)
	public Long PendingAssignedByYou(String email);

	// ToYou
	@Query(value = "SELECT COUNT(*) FROM projects p, assignedto u, projects_assigned_to z  WHERE u.email=?1 AND z.project_id=p.id AND z.assigned_to_id=u.id", nativeQuery = true)
	public Long TotalProjectsAssignedToYou(String email);

	@Query(value = "SELECT COUNT(*) FROM projects p, assignedto u, projects_assigned_to z  WHERE (select cast(now() as date))< p.start_date AND p.completion_percent=0 AND u.email=?1 AND z.project_id=p.id AND z.assigned_to_id=u.id", nativeQuery = true)
	public Long ToDoProjectsAssignedToYou(String email);

	@Query(value = "SELECT COUNT(*) FROM projects p, assignedto u, projects_assigned_to z  WHERE (select cast(now() as date))>= p.start_date AND (select cast(now() as date))<= p.deadline AND p.completion_percent>0 AND p.completion_percent<100 AND u.email=?1 AND z.project_id=p.id AND z.assigned_to_id=u.id", nativeQuery = true)
	public Long OnTrackProjectsAssignedToYou(String email);

	@Query(value = "SELECT COUNT(*) FROM projects p , assignedto u, projects_assigned_to z  WHERE p.completion_percent=100 AND u.email=?1 AND z.project_id=p.id AND z.assigned_to_id=u.id", nativeQuery = true)
	public Long CompletedProjectsAssignedToYou(String email);

	@Query(value = "SELECT COUNT(*) FROM projects p, assignedto u, projects_assigned_to z  WHERE (select cast(now() as date)) >= p.start_date AND p.completion_percent=0 AND u.email=?1 AND z.project_id=p.id AND z.assigned_to_id=u.id", nativeQuery = true)
	public Long PendingProjectsAssignedToYou(String email);

}
