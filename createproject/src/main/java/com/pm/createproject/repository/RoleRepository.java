package com.pm.createproject.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pm.createproject.entity.ERoleAssignedTo;
import com.pm.createproject.entity.RoleAssignedTo;

@Repository
public interface RoleRepository extends JpaRepository<RoleAssignedTo, Long> {
	Optional<RoleAssignedTo> findByName(ERoleAssignedTo name);
}
