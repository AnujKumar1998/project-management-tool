package com.pm.createproject.services;

import java.util.List;

import com.pm.createproject.entity.AssignedBy;

public interface AssignedbyService {
	 public List<AssignedBy> getAllTheUsers();
     public AssignedBy getUserById(Long id);
     public AssignedBy getUserByName(String name);
     public void deleteTheUser(Long id);
     public List<AssignedBy> searchTheUsers(String keyword);
     public AssignedBy saveTheUser(AssignedBy assignedby);
}
