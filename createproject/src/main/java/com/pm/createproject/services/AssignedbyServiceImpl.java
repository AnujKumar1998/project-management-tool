package com.pm.createproject.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pm.createproject.entity.AssignedBy;
import com.pm.createproject.repository.AssignedbyRepository;

@Service
@Transactional
public class AssignedbyServiceImpl implements AssignedbyService {

	@Autowired
	private AssignedbyRepository assignedbyrepository;

	@Override
	public List<AssignedBy> getAllTheUsers() {
		return assignedbyrepository.findAll();
	}

	@Override
	public AssignedBy getUserById(Long id) {
		return assignedbyrepository.findById(id).get();
	}

	@Override
	public AssignedBy getUserByName(String name) {
		return assignedbyrepository.findByName(name);
	}

	@Override
	public void deleteTheUser(Long id) {
		assignedbyrepository.deleteById(id);
	}

	@Override
	public List<AssignedBy> searchTheUsers(String keyword) {
		return assignedbyrepository.searchTheUsers(keyword);
	}

	@Override
	public AssignedBy saveTheUser(AssignedBy assignedby) {
		return assignedbyrepository.save(assignedby);
	}

}
