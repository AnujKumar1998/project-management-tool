package com.pm.createproject.services;

import java.util.List;

import com.pm.createproject.entity.AssignedTo;

public interface AssignedtoService {
	 public List<AssignedTo> getAllTheUsers();
     public AssignedTo getUserById(Long id);
     public AssignedTo getUserByName(String name);
     public void deleteTheUser(Long id);
     public List<AssignedTo> searchTheUsers(String keyword);
     
     public AssignedTo saveTheUser(AssignedTo assignedTo);
}
