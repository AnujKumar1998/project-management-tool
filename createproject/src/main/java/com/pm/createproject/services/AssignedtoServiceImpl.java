package com.pm.createproject.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pm.createproject.entity.AssignedTo;
import com.pm.createproject.repository.AssignedtoRepository;

@Service
@Transactional
public class AssignedtoServiceImpl implements AssignedtoService {

	@Autowired
	private AssignedtoRepository userrepository;

	@Override
	public List<AssignedTo> getAllTheUsers() {
		return userrepository.findAll();
	}

	@Override
	public AssignedTo getUserById(Long id) {
		return userrepository.findById(id).get();
	}

	@Override
	public AssignedTo getUserByName(String name) {
		return userrepository.findByName(name);
	}

	@Override
	public void deleteTheUser(Long id) {

		userrepository.deleteById(id);
	}

	@Override
	public List<AssignedTo> searchTheUsers(String keyword) {
		return userrepository.searchTheUsers(keyword);
	}

	@Override
	public AssignedTo saveTheUser(AssignedTo assignedTo) {
		return userrepository.save(assignedTo);
	}

}
