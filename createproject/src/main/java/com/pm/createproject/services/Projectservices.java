package com.pm.createproject.services;


import java.util.Date;
import java.util.List;

import com.pm.createproject.entity.Project;

public interface Projectservices {
	    public List<Project> getAllProjects();

	    public Project getProjectById(Long id);
	    
	    public Project getProjectByName(String name);

	    public Project saveProject(Project project);

	    public void deleteProject(Long id);
	    
	    public void updateProjectCompletionPercent(Integer  completion_percent, Long id);
	    
	    public void updateTheProject(String name, String description, Date created_on, Date start_date,Date deadline, String assigned_to, String assigned_by, Integer completion_percent, Long id);
	    
	    public List<Project> allProjectsAssignedToYou(String email);
	    
		public List<Project> allProjectsAssignedByYou(String email);
		
		
		public Long TotalProjectsAssignedByYou(String email); 
		public Long ToDoProjectsAssignedByYou(String email); 
		public Long OnTrackProjectsAssignedByYou(String email); 
		public Long CompletedProjectsAssignedByYou(String email); 
		public Long PendingProjectsAssignedByYou(String email); 
		
		
		public Long TotalProjectsAssignedToYou(String email); 
		public Long ToDoProjectsAssignedToYou(String email); 
		public Long OnTrackProjectsAssignedToYou(String email); 
		public Long CompletedProjectsAssignedToYou(String email); 
		public Long PendingProjectsAssignedToYou(String email); 
		
		public List<Project> Search(String keyword);

}
