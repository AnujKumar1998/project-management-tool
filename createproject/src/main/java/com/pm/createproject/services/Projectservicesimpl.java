package com.pm.createproject.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pm.createproject.entity.Project;
import com.pm.createproject.repository.ProjectRepository;

@Service
@Transactional
public class Projectservicesimpl implements Projectservices {

	@Autowired
	private ProjectRepository projectRepository;

	// Projects------------------------------------>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	@Override
	public List<Project> getAllProjects() {

		List<Project> allProjects = projectRepository.findAll();
		return allProjects;
	}

	@Override
	public Project getProjectById(Long Id) {

		Project project = projectRepository.findById(Id).get();
		return project;
	}

	@Override
	public Project saveProject(Project project) {

//		for(User i: project.getAssigned_to())
//		{   
//			userserviceimpl.saveTheUser(i);
//			i.setProject(project);
//		}

		return projectRepository.save(project);
	}

	@Override
	public void deleteProject(Long id) {
//       Project project=projectRepository.findById(id).get();
//       
//       AssignedBy assignedby=project.getAssigned_by();
//       assignedbyservice.deleteTheUser(assignedby.getId());
//       
//       List<AssignedTo> assignedto=project.getAssigned_to();
//       
//       for(AssignedTo assignedtoo: assignedto)
//       {
//    	   assignedtoservice.deleteTheUser(assignedtoo.getId());
//       }

		projectRepository.deleteById(id);
	}

	@Override
	public Project getProjectByName(String name) {
		return projectRepository.findByName(name);
	}

	@Override
	public void updateProjectCompletionPercent(Integer completion_percent, Long id) {
		projectRepository.updateProjectCompletionPercent(completion_percent, id);
	}

	@Override
	public void updateTheProject(String name, String description, Date created_on, Date start_date, Date deadline,
			String assigned_to, String assigned_by, Integer completion_percent, Long id) {
		projectRepository.updateProject(name, description, created_on, start_date, deadline, assigned_to, assigned_by,
				completion_percent, id);
	}

	@Override
	public List<Project> allProjectsAssignedToYou(String email) {

		return projectRepository.allProjectsAssignedToYou(email);
	}

	@Override
	public List<Project> allProjectsAssignedByYou(String email) {

		return projectRepository.allProjectsAssignedByYou(email);
	}

	@Override
	public List<Project> Search(String keyword) {
		if (keyword != null) {
			return projectRepository.search(keyword);
		}
		return projectRepository.findAll();
	}

	// Dashboard----------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	@Override
	public Long TotalProjectsAssignedByYou(String email) {
		return projectRepository.TotalProjectsAssignedByYou(email);
	}

	@Override
	public Long ToDoProjectsAssignedByYou(String email) {
		return projectRepository.ToDoAssignedByYou(email);
	}

	@Override
	public Long OnTrackProjectsAssignedByYou(String email) {
		return projectRepository.OnTrackAssignedByYou(email);
	}

	@Override
	public Long CompletedProjectsAssignedByYou(String email) {
		return projectRepository.CompletedAssignedByYou(email);
	}

	@Override
	public Long PendingProjectsAssignedByYou(String email) {
		return projectRepository.PendingAssignedByYou(email);
	}

	@Override
	public Long TotalProjectsAssignedToYou(String email) {
		return projectRepository.TotalProjectsAssignedToYou(email);
	}

	@Override
	public Long ToDoProjectsAssignedToYou(String email) {
		return projectRepository.ToDoProjectsAssignedToYou(email);
	}

	@Override
	public Long OnTrackProjectsAssignedToYou(String email) {
		return projectRepository.OnTrackProjectsAssignedToYou(email);
	}

	@Override
	public Long CompletedProjectsAssignedToYou(String email) {
		return projectRepository.CompletedProjectsAssignedToYou(email);
	}

	@Override
	public Long PendingProjectsAssignedToYou(String email) {
		return projectRepository.PendingProjectsAssignedToYou(email);
	}

}
