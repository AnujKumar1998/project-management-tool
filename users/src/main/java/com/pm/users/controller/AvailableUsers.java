package com.pm.users.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pm.users.entity.User;
import com.pm.users.feignClient.AvailableUsersClient;
import com.pm.users.headers.HeaderGenerator;
import com.pm.users.services.UsersService;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = "/users/available-users")
public class AvailableUsers {
      
	@Autowired
	private UsersService userService;
   
	@Autowired
	private HeaderGenerator headerGenerator;
	
	@Autowired
	private AvailableUsersClient availableusersclient;
	
	@GetMapping(value = "")
	public ResponseEntity<List<User>> getAllUsers() {
		return availableusersclient.getAllTheUsers();	
	}
	
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<User> getUser(@PathVariable("id") Long id) {
		  return availableusersclient.getTheUserById(id);
	}
	
	@GetMapping(value = "/",params = "name")
	public ResponseEntity<User> getUser(@RequestParam("name") String name) {
		   return availableusersclient.getTheUserByName(name);
		
	}
	
	@PostMapping(value = "/save/{id}")
	public ResponseEntity<User> addUser(@PathVariable Long id, HttpServletRequest request) {
       User user = availableusersclient.getTheUserById(id).getBody();
		if(user != null)
    		try {
    			userService.saveTheUSer(user);
    			return new ResponseEntity<User>(
    					user,
    					headerGenerator.getHeadersForSuccessPostMethod(request, user.getId()),
    					HttpStatus.CREATED);
    		}catch (Exception e) {
    			e.printStackTrace();
    			return new ResponseEntity<User>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    	return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);

	}
	
	@GetMapping(value = "/search/{keyword}")
	public ResponseEntity<List<User>> searchTheUsers(@PathVariable String keyword) {
		return availableusersclient.searchTheUsers(keyword);	
	}
	
}
