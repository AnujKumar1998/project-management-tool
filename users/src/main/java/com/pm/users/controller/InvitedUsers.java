package com.pm.users.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pm.users.entity.User;
import com.pm.users.headers.HeaderGenerator;
import com.pm.users.services.UsersService;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = "/users/invited-users")
public class InvitedUsers {
    
	@Autowired
	private UsersService userService;

	@Autowired
	private HeaderGenerator headerGenerator;

	//Users------------------------------------------>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	
	@GetMapping(value = "")
	public ResponseEntity<List<User>> getAllUsers() {
		List<User> users = userService.getAllTheUsers();
		if (!users.isEmpty()) {
			return new ResponseEntity<List<User>>(users, headerGenerator.getHeadersForSuccessGetMethod(),
					HttpStatus.OK);
		}
		return new ResponseEntity<List<User>>(headerGenerator.getHeadersForError(), HttpStatus.NOT_FOUND);
	}
	
	
	
	

	@GetMapping(value = "/{id}")
	public ResponseEntity<User> getUser(@PathVariable("id") Long id) {
		    User user = userService.getUserById(id);
	
	        if(user != null) {
	    		return new ResponseEntity<User>(
	    				user,
	    				headerGenerator.
	    				getHeadersForSuccessGetMethod(),
	    				HttpStatus.OK);
	    	}
	        return new ResponseEntity<User>(
	        		headerGenerator.getHeadersForError(),
	        		HttpStatus.NOT_FOUND);
		
	}
	
	@GetMapping(value = "/",params = "name")
	public ResponseEntity<User> getUser(@RequestParam("name") String name) {
		    User user = userService.getUserByName(name);
	
	        if(user != null) {
	    		return new ResponseEntity<User>(
	    				user,
	    				headerGenerator.
	    				getHeadersForSuccessGetMethod(),
	    				HttpStatus.OK);
	    	}
	        return new ResponseEntity<User>(
	        		headerGenerator.getHeadersForError(),
	        		HttpStatus.NOT_FOUND);
		
	}
	
	
	

	@DeleteMapping(value = "/delete/{id}")
	public ResponseEntity<Void> removeUser(@PathVariable("id") Long id) {
		 User user = userService.getUserById(id);
    	if(user != null) {
    		try {
    			userService.deleteTheUser(id);
    	        return new ResponseEntity<Void>(
    	        		headerGenerator.getHeadersForSuccessGetMethod(),
    	        		HttpStatus.OK);
    		}catch (Exception e) {
				e.printStackTrace();
    	        return new ResponseEntity<Void>(
    	        		headerGenerator.getHeadersForError(),
    	        		HttpStatus.INTERNAL_SERVER_ERROR);
			}
    	}
    	return new ResponseEntity<Void>(headerGenerator.getHeadersForError(), HttpStatus.NOT_FOUND);    
        
	}
	
	@GetMapping(value = "/search/{keyword}")
	public ResponseEntity<List<User>> searchTheUsers(@PathVariable String keyword) {
		List<User> users = userService.searchTheUsers(keyword);
		if (!users.isEmpty()) {
			return new ResponseEntity<List<User>>(users, headerGenerator.getHeadersForSuccessGetMethod(),
					HttpStatus.OK);
		}
		return new ResponseEntity<List<User>>(headerGenerator.getHeadersForError(), HttpStatus.NOT_FOUND);
	}
	
}
