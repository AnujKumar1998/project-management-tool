package com.pm.users.feignClient;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.pm.users.entity.User;


@FeignClient(name = "Admin")
public interface AvailableUsersClient {
      
	@GetMapping(value = "/admin/active-users/getall")
	public ResponseEntity<List<User>> getAllTheUsers();

	@GetMapping(value = "/admin/active-users/{id}")
	public ResponseEntity<User> getTheUserById(@PathVariable("id") Long id);

	@GetMapping(value = "/admin/active-users/", params = "name")
	public ResponseEntity<User> getTheUserByName(@RequestParam("name") String name);

	@DeleteMapping(value = "/admin/active-users/delete/{id}")
	public ResponseEntity<Void> deleteTheUser(@PathVariable("id") Long id);

	@GetMapping(value = "/admin/active-users/search/{keyword}")
    public ResponseEntity<List<User>> searchTheUsers(@PathVariable("keyword") String keyword);
}
