package com.pm.users.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pm.users.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	Optional<User> findByUsername(String username);

	User findByName(String name);

	@Query(value = "SELECT * FROM users u WHERE CONCAT(u.username,u.name,u.email,u.mobno,u.dept,u.empid) LIKE %?1%", nativeQuery = true)
	public List<User> searchTheUsers(String keyword);
}
