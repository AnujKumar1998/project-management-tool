package com.pm.users.services;

import java.util.List;

import com.pm.users.entity.User;

public interface UsersService {
	 public List<User> getAllTheUsers();
     public User getUserById(Long id);
     public User getUserByName(String name);
     public void deleteTheUser(Long id);
     public List<User> searchTheUsers(String keyword);
     
     public User saveTheUSer(User user);
}
